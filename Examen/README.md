1/ Les étapes nécessaires pour supprimer un élément d'une liste chaînée en connaissant son adresse sont :
 Trouver le nœud précédent l'élément à supprimer.
 Mettre à jour le champ "next" du nœud précédent pour qu'il pointe vers le nœud suivant de celui à supprimer.
 Libérer la mémoire occupée par le nœud à supprimer.
 
 
2/ Les différences notables entre les deux fonctions sont :
 La première, "remove_list_entry_bad_taste", nécessite de parcourir la liste jusqu'à trouver l'élément à supprimer et son prédécesseur, tandis que la seconde, "remove_list_entry_good_taste", utilise un pointeur vers un pointeur pour pouvoir modifier directement le champ "next" du nœud précédent.
 La seconde fonction renvoie un pointeur vers le nœud suivant de celui qui a été supprimé, car elle peut modifier directement le champ "next" du nœud précédent, tandis que la première doit renvoyer le pointeur vers la tête de la liste pour prendre en compte le cas où l'élément à supprimer est en tête de liste.


3/ La première fonction ne modifie pas directement le pointeur vers la tête de liste, elle utilise une variable locale pour stocker la nouvelle valeur du pointeur et la renvoie à la fin de la fonction. La seconde fonction, en revanche, peut modifier directement le pointeur vers la tête de liste grâce à l'utilisation d'un pointeur vers un pointeur.

4/ Un pointeur vers un pointeur permet de manipuler l'adresse d'un pointeur à partir d'une fonction, ce qui est nécessaire dans la seconde fonction pour pouvoir modifier directement le champ "next" du nœud précédent.

/5 La seconde fonction reçoit un pointeur vers un pointeur comme argument car elle a besoin de modifier directement le champ "next" du nœud précédent, ce qui implique de manipuler l'adresse du pointeur vers le nœud précédent.

6/ Selon moi, la seconde fonction est plus élégante car elle permet une suppression plus directe et efficace d'un élément de la liste, sans avoir besoin de parcourir la liste pour trouver son prédécesseur. De plus, elle utilise un pointeur vers un pointeur, qui est une technique courante en programmation pour manipuler des pointeurs dans des structures de données complexes.
