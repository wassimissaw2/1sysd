#include <stdio.h>

#define MAX_LENGTH 50


int count_char(char *string, char c) {
    int count = 0;
    for (int i = 0; string[i] != '\0'; i++) {
        if (string[i] == c) {
            count++;
        }
    }
    return count;
}


int count_words(char *string, char *word) {
    int count = 0;
    int i = 0;
    while (string[i] != '\0') {
        if (string[i] != ' ' && (i == 0 || string[i-1] == ' ')) {
            int j = 0;
            while (word[j] != '\0' && string[i+j] == word[j]) {
                j++;
            }
            
            if (word[j] == '\0') {
                count++;
            }
        }
        i++;
    }
    return count;
}

int count_words_better(char *string, char *word) {
    int count = 0;
    int i = 0;
    while (string[i] != '\0') {
        if (string[i] != ' ') {
            int j = 0;
            while (word[j] != '\0' && string[i+j] == word[j]) {
                j++;
            }
            if (word[j] == '\0') {
                count++;
            }
            while (string[i+j] != '\0' && string[i+j] != ' ') {
                j++;
            }
        }
        i++;
    }
    return count;
}

int main() {
    char string[MAX_LENGTH];
    printf("Entrez une chaine de caracteres (max %d caracteres) : ", MAX_LENGTH-1);
    fgets(string, MAX_LENGTH, stdin);

    int i = 0;
    while (string[i] != '\n') {
        i++;
    }
    string[i] = '\0';

    char c = 'o';
    int char_count = count_char(string, c);
    printf("Le caractere '%c' apparait %d fois dans la chaine.\n", c, char_count);

    char word[] = "be";
    int word_count = count_words(string, word);
    printf("La chaine contient %d mots.\n", word_count);

    int word_count_better = count_words_better(string, word);
    printf("La chaine contient %d mots (version amelioree).\n", word_count_better);

    return 0;

