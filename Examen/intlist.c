#include <stdio.h>
#include <stdlib.h>

typedef struct node node;
struct node {
    int val;
    node *next;
    node *prev;
};

node *create_node(int val) {
    node *p = malloc(sizeof(node));
    p->val = val;
    p->next = NULL;
    p->prev = NULL;  // initialise le champ "prev" à NULL
    return p;
}

void print_list(node *head) {
    node *walk = head;
    while (walk != NULL) {
        printf("%d ", walk->val);
        walk = walk->next;
    }
    printf("\n");
}

node *append_val(node *head, int val) {
    node *newnode = create_node(val);
    if (head == NULL) {  // liste vide
        head = newnode;
    } else {
        node *walk = head;
        while (walk->next != NULL) {
            walk = walk->next;
        }
        walk->next = newnode;
        newnode->prev = walk;  // met à jour le champ "prev"
    }
    return head;
}

void forth_and_back(node *head) {
    // parcourt la liste du premier au dernier élément
    node *walk = head;
    while (walk != NULL) {
        printf("%d ", walk->val);
        walk = walk->next;
    }
    printf("\n");
    // parcourt la liste du dernier au premier élément
    walk = head;
    while (walk->next != NULL) {
        walk = walk->next;
    }
    while (walk != NULL) {
        printf("%d ", walk->val);
        walk = walk->prev;
    }
    printf("\n");
}

int main() {
    node *head1 = NULL;
    node *head2 = NULL;

    // Ajoute des éléments à la liste
    head1 = append_val(head1, 42);
    head1 = append_val(head1, 12);
    head1 = append_val(head1, 54);
    head1 = append_val(head1, 41);

    // Affiche la liste du premier au dernier élément, puis du dernier au premier
    forth_and_back(head1);

    return 0;
}

