#include<stdio.h>
#include<stdlib.h>
#include<math.h>


void add_vectors(int n, double *t1, double *t2, long double *t3) {
   for(int i = 0; i < n; i++) {
      double diff = t1[i] - t2[i];
      t3[i] = pow(diff, 2);
   }
}

double norm_vectors(int n, double *t) {
   double sum_of_squares = 0;
   for(int i = 0; i < n; i++) {
      sum_of_squares += pow(t[i], 2);
   }
   return sqrt(sum_of_squares);
}


int main() {

	double T1[] = { 3.14, -1.0, 2.3, 0, 7.1 };
	double T2[] = { 2.71,  2.5,  -1, 3, -7  };
	double T3[5];
	int n = 5;
	add_vectors(5, T1, T2, T3);

	printf("T1 : ");
	for (int i = 0; i < n; i++) {
		printf("%+.2lf ", T1[i]);
	}
	printf("\nT2 : ");
	for (int i = 0; i < n; i++) {
		printf("%+.2lf ", T2[i]);
	}
	printf("\n");
	
	printf("T3 = { ");
   	for (int i = 0; i < 5; i++) {
      	printf("%.2Lf ", T3[i]);
  	 }
  	 printf("}\n");

   	double norm = norm_vector(5, T3);
   	rintf("Norm of T3 = %.2f\n", norm);

  	 return 0;
	}	
	
	exit(EXIT_SUCCESS);

}

